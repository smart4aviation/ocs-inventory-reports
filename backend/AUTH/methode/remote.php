<?php
//====================================================================================
// OCS INVENTORY REPORTS
// Copyleft Erwan GOALOU 2010 (erwan(at)ocsinventory-ng(pt)org)
// Web: http://www.ocsinventory-ng.org
//
// This code is open source and may be copied and modified as long as the source
// code is always made freely available.
// Please refer to the General Public Licence http://www.gnu.org/ or Licence.txt
//====================================================================================

$login = $_SERVER['REMOTE_USER'];
$mdp = '';
$user_group = '';
$_SESSION['OCS']['details']['givenname'] = $_SERVER['AUTHENTICATE_GIVENNAME'];
$_SESSION['OCS']['details']['sn'] = $_SERVER['AUTHENTICATE_SN'];
$_SESSION['OCS']['details']['mail'] = $_SERVER['AUTHENTICATE_MAIL'];
$_SESSION['OCS']['sadmin'] = 'sadmin';
$cnx_origine = 'LDAP';
$config['LDAP_CHECK_DEFAULT_ROLE'] = 'sadmin';
$login_successful="OK";
