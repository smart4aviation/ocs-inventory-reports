<?php
//==============================================================================
// cd_officepack.php
// Plugin OCS MsOfficeKey
// Nicolas DEROUET (nicolas.derouet(at)gmail(pt)com)
// 15/02/2013 14:20
//==============================================================================

  $form_name="officepack"; 
  print_item_header("Gestionnaire des licences Microsoft Office");
  
  if (!isset($protectedPost['SHOW'])) $protectedPost['SHOW'] = 'SHOW';
  
  $table_name=$form_name;
  echo "<form name='".$form_name."' id='".$form_name."' method='post' action=''>";
  $list_fields=array
  (
    '&nbsp;' => 'INS',
    'Version' => 'OFFICEVERSION',
    'Produit' => 'PRODUCT',
    'Type' => 'TYPE',
    'Licence' => 'OFFICEKEY',                     
    'ProductID' => 'PRODUCTID',
    'GUID' => 'GUID',
    'GUID: Produit' => 'GUID_PRODUCT',
    'GUID: Version' => 'GUID_VERSION',
    'GUID: Type' => 'GUID_TYPE',
    'GUID: Langue' => 'GUID_LANG',
    'Note' => 'NOTE'                     
  );
  
  if($show_all_column)
  	$list_col_cant_del=$list_fields;
  else
  	$list_col_cant_del=array('&nbsp;' => 'INS','Produit' => 'PRODUCT');
  
  $default_fields=array
  (
    '&nbsp;' => 'INS',
    'Version' => 'OFFICEVERSION',
    'Produit' => 'PRODUCT',
    'Type' => 'TYPE',
    'Licence' => 'OFFICEKEY'
  );
  
  $tab_options['OTHER']['&nbsp;']['<!--1-->'] = "1";
  $tab_options['OTHER']['IMG'] = "image/green.png";
  $tab_options['OTHER_BIS']['&nbsp;']['<!--0-->'] = "0";
  $tab_options['OTHER_BIS']['IMG'] = "image/red.png";
  
  $queryDetails  = "
    SELECT officepack.*, CONCAT('<!--', INSTALL, '-->') AS INS,
    T_GUID.LANG as GUID_LANG, T_GUID.PRODUCT AS GUID_PRODUCT, T_GUID.TYPE_VERSION as GUID_TYPE, T_GUID.VERSION as GUID_VERSION
    FROM officepack
    LEFT JOIN ( 
      SELECT officepack.GUID, officepack_lang.LANG, officepack_sku.PRODUCT, officepack_type.TYPE_VERSION, officepack_version.VERSION
      FROM officepack
      LEFT JOIN officepack_lang ON substring(GUID,6,4) = officepack_lang.LCID
      LEFT JOIN officepack_sku ON substring(GUID,4,2) = officepack_sku.REF_ID AND officepack.OFFICEVERSION = officepack_sku.VERSION
      LEFT JOIN officepack_type ON substring(GUID,3,1) = officepack_type.REF_ID AND OFFICEVERSION <> '2000' 
      LEFT JOIN officepack_version ON substring(GUID,2,1) = officepack_version.REF_ID AND OFFICEVERSION <> '2000'
      WHERE (OFFICEVERSION = '2000' AND substring(GUID,11,27) = '78E1-11D2-B60F-006097C998E7')
      OR    (OFFICEVERSION = 'XP'   AND substring(GUID,11,27) = '6000-11D3-8CFE-0050048383C9')
      OR    (OFFICEVERSION = '2003' AND substring(GUID,11,27) = '6000-11D3-8CFE-0150048383C9')  
     UNION
      SELECT officepack.GUID, officepack_lang.LANG, officepack_sku.PRODUCT, officepack_type.TYPE_VERSION, officepack_version.VERSION
      FROM officepack
      LEFT JOIN officepack_lang ON substring(GUID,16,4) = officepack_lang.LCID
      LEFT JOIN officepack_sku ON substring(GUID,11,4) = officepack_sku.REF_ID AND officepack.OFFICEVERSION = officepack_sku.VERSION
      LEFT JOIN officepack_type ON substring(GUID,3,1) = officepack_type.REF_ID 
      LEFT JOIN officepack_version ON substring(GUID,2,1) = officepack_version.REF_ID
      WHERE OFFICEVERSION IN ('2007','2010','2013') AND substring(GUID,27,11) = '000000FF1CE'
    ) as T_GUID ON officepack.GUID = T_GUID.GUID
    WHERE (hardware_id = $systemid)";
  
  tab_req($table_name,$list_fields,$default_fields,$list_col_cant_del,$queryDetails,$form_name,80,$tab_options);
  echo "</form>";
?>